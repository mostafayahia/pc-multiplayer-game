﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    public Image PreviewImage;
    public List<Sprite> TutImages;
    public List<string> textTutorial; 
    public Text ImageDescTxt; 
               private int imageCounter ;
    public GameObject btnForward, btnPrevious;
    public GameObject tutorialOBject; 
    public void HowToPlayBTN ()
    {
        if (tutorialOBject.activeSelf) tutorialOBject.SetActive(false);
        else tutorialOBject.SetActive(true);
    }
    public void NextPreviousBtn (int value)
    {
        switch (value)
        {
            case 0:
              imageCounter++;
              
                break;
            case 1:
              imageCounter--;
                break; 
        }
        PreviewImage.sprite = TutImages[imageCounter];
        ImageDescTxt.text = textTutorial[imageCounter];
    }
    private void Update()
    {
        if (imageCounter == 0) btnPrevious.SetActive(false);
        else btnPrevious.SetActive(true);
        if (imageCounter == TutImages.Count-1) btnForward.SetActive(false);
        else btnForward.SetActive(true);
    }
}
