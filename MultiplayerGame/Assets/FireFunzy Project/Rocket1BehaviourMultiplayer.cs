﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun   ;

public class Rocket1BehaviourMultiplayer : MonoBehaviourPunCallbacks, IPunObservable
{
    public static Transform CarTransform;
    public static int Team1Score, Team2Score;
    [SerializeField] GameObject HitEffect, CarHitAudEffect;
    public static string thisRocketTag;
    private void Awake()
    {

        CarTransform = GameManagerMultiplayer.Instance.playerPrefabs[0].transform;
        Debug.Log("Car Pos = " + GameManagerMultiplayer.Instance.playerPrefabs[0].transform.position);
        VehicleControllerMultiplayer.THECAR = GameObject.FindGameObjectWithTag("Car").transform;
    }
    private void Start()
    {
        //   transform.LookAt(VehicleController.THECAR);
        this.gameObject.tag = thisRocketTag;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward);
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        throw new System.NotImplementedException();
    }
    public Collider guardCubeCollider;
    private void OnTriggerEnter(Collider other)
    {
        if (photonView.IsMine)
        {
            if (other.tag == "Car")
            {

                PhotonNetwork.Instantiate(HitEffect.name, transform.position, Quaternion.identity);
                PhotonNetwork.Instantiate(CarHitAudEffect.name, transform.position, Quaternion.identity);

                if (this.gameObject.tag == "Man" || this.gameObject.tag == "PoliceMan")
                {
                    if (VehicleControllerMultiplayer.EnableGuradCubeEffect)
                        ;
                    else
                    {
                        if (GameManagerMultiplayer.Rounds == 2 || GameManagerMultiplayer.Rounds == 4)
                        {
                            Team1Score++;
                        }
                    }


                    photonView.RPC("TeamScore", RpcTarget.All, Team1Score, Team2Score);
                    Debug.Log(this.gameObject.tag + Team1Score);
                }
                if (this.gameObject.tag == "Girl" || this.gameObject.tag == "Zombie")
                {
                    if (VehicleControllerMultiplayer.EnableGuradCubeEffect)
                        ;
                    else
                    {
                        if (GameManagerMultiplayer.Rounds == 1 || GameManagerMultiplayer.Rounds == 3)
                        {
                            Team2Score++;
                        }
                    }
                    photonView.RPC("TeamScore", RpcTarget.All, Team1Score, Team2Score);

                    Debug.Log(this.gameObject.tag + Team2Score);
                }
                Debug.Log("my tag = " + this.gameObject.tag);
                //  Team1Score++;
                Destroy(this.gameObject);
            }
            if (other.tag == "GuardCube")
            {
                guardCubeCollider = other;

                Destroy(this.gameObject);
                Destroy(other.gameObject);
                photonView.RPC("GuardCube", RpcTarget.All, other.GetComponent<PhotonView>().ViewID);
            }
        }




    }
    [PunRPC]
    void TeamScore(int team1score, int team2score)
    {
        Debug.Log("team 2 score  = " + team2score);
        Team1Score = team1score;
        Team2Score = team2score;
        Destroy(this.gameObject);
    }
    [PunRPC]
    void GuardCube(int id)
    {
        Debug.Log("id = " + id);
        if (GameManagerMultiplayer.Rounds == 1 || GameManagerMultiplayer.Rounds == 3)
        {
            if (this.gameObject.tag == "Man" || this.gameObject.tag == "PoliceMan")
            {
                VehicleControllerMultiplayer.EnableGuradCubeEffect = true;

            }
        }
        if (GameManagerMultiplayer.Rounds == 2 || GameManagerMultiplayer.Rounds == 4)
        {
            if (this.gameObject.tag == "Girl" || this.gameObject.tag == "Zombie")
            {
                VehicleControllerMultiplayer.EnableGuradCubeEffect = true;

            }                                                                                                                                               
        }
        //guardCubeCollider = Other; 
        Destroy(PhotonView.Find(id).gameObject);
        Destroy(this.gameObject);


    }
}
