﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;

public class GameManagerMultiplayer : MonoBehaviourPunCallbacks, IPunObservable
{
    bool char2;
    bool char1;
    int NumOFplayer;
    public static GameObject LocalPlayer;
    public static int Team1Counter, Team2Counter;
    [SerializeField] Text txtTeam2, txtTeam1, TimerTxt, PhaseTxt, TextToshowSomeValues, WinLosetxt;
    float Timer = 30f;
    public static int Rounds;
    public int PhaseValue;
    public GameObject LoadingScreen;
    public GameObject MainCanvas;
    public GameObject GuardCubed;
    #region Public Fields

    static public GameManagerMultiplayer Instance;

    #endregion

    #region Private Fields



    [Tooltip("The prefab to use for representing the player")]
    [SerializeField]
    public List<GameObject> playerPrefabs;
    [SerializeField] GameObject Player2SourcePos, PoliceManSourcePos, GirlSourcePos, ZombieSourcePos, WinnersoundObject, LoserSoundObject;
    #endregion

    #region MonoBehaviour CallBacks

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    void Start()
    {
        SendPhaseValues();
        TextToshowSomeValues.text = (" Id = " + photonView.ViewID);

        InvokeRepeating("SpawnGuardCubesRandomly", 3, 8);


        //  StartCoroutine(CarMode());
        Instance = this;

        // in case we started this demo with the wrong scene being active, simply load the menu scene
        if (!PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene("PunBasics-Launcher");

            return;
        }

        if (GetPlayerPrefab() == null)
        { // #Tip Never assume public properties of Components are filled up properly, always check and inform the developer of it.

            Debug.LogError("<Color=Red><b>Missing</b></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else
        {


            if (VehicleControllerMultiplayer.LocalPlayerInstance == null)
            {
                Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);

                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            }
            else
            {

                Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
            }


        }

    }

    void SpawnGuardCubesRandomly()
    {
        if (Rounds != 0 && countertostartgame)
        {
            PhotonNetwork.Instantiate(GuardCubed.name, new Vector3(Random.Range(-47, -35), 1.17f, Random.Range(10.5f, 22)), Quaternion.identity);

        }
    }
    private void OnGUI()
    {
        //Debug.Log("OnGUI Work");
        //GUI.TextField(new Rect(50, 50, 150, 100), char2+" " + char1);
        //GUI.TextField(new Rect(500, 50, 150, 100), NumOFplayer.ToString());

    }
    GameObject GetPlayerPrefab()
    {
        for (int i = 0; i < playerPrefabs.Count; i++)
        {
            return playerPrefabs[i];
        }
        return null;
    }
    int prefab;
    int GetRandomPrefab()
    {
        prefab = Random.Range(0, 2);

        return prefab;
    }
    int getRandomNum()
    {
        Debug.Log("Get Random Num");
        int number = 0;


        if (!char2 && !char1)
        {
            number = Random.Range(1, 3);

        }
        else if (char2 && !char1)
        {
            number = 2;

        }
        else if (char1 && !char2)
        {
            number = 1;

        }
        // TextToshowSomeValues.text = "Num = " + number.ToString() + "CHAR 1 = " +  char1 + " char 2 = " + char2; 
        if (number == 1)
            photonView.RPC("ChangeBool", RpcTarget.AllBuffered, true, false);
        if (number == 2)
            photonView.RPC("ChangeBool", RpcTarget.AllBuffered, false, true);
        return number;
    }
    Vector3 GetPlayerPos(int value)
    {
        switch (value)
        {
            case 0:
                return Vector3.zero;


            case 1:
                return Player2SourcePos.transform.position;

            case 2:
                return PoliceManSourcePos.transform.position;

            case 3:
                return GirlSourcePos.transform.position;
                break;
            case 4:
                return ZombieSourcePos.transform.position;
                break;
            default:
                return new Vector3(-43.01f, 0, 17.6f);

        }

    }
    GameObject TempCanvas;
    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity on every frame.
    /// </summary>
    void Update()
    {
        //  if (photonView.IsMine)
        // {
        if (PhotonNetwork.CurrentRoom != null)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount != 4 && Rounds > 0)
            {
                Debug.LogWarning("Some One Out  " + PlayerLeft());

                photonView.RPC("PlayerLeaveAgame", RpcTarget.All, PlayerLeft());
                //StartCoroutine(GoToMainMenu());
            }
        }
        //}
        // WinLosetxt.text = VehicleController.playertagnameforwinlose; 
        if (Timer <= 0)
        {

            if (GameObject.FindGameObjectWithTag("Car") != null)
            {
                GameObject.FindGameObjectWithTag("Car").SetActive(false);
            }
            Timer = 30;
            startcourotine = false;
            thecountertostartgame = 0;
            countertostartgame = false;
            //   StartCoroutine(CounterToStartGame());



            for (int i = 0; i < GameObject.FindGameObjectWithTag(VehicleControllerMultiplayer.tagname).transform.childCount; i++)
            {
                GameObject.FindGameObjectWithTag(VehicleControllerMultiplayer.tagname).GetComponent<VehicleControllerMultiplayer>().enabled = true; // Here to Enable  Movement of the player after  using the car 
                VehicleControllerMultiplayer.setcam = true;
                GameObject.FindGameObjectWithTag(VehicleControllerMultiplayer.tagname).transform.GetChild(i).gameObject.SetActive(true);
                if (VehicleControllerMultiplayer.playerlocalCanvas != null) VehicleControllerMultiplayer.playerlocalCanvas.SetActive(true);
                if (VehicleControllerMultiplayer.LocalCarCanvas != null) VehicleControllerMultiplayer.LocalCarCanvas.SetActive(false);
            }

        }
        if (countertostartgame)
        {
            Timer -= Time.deltaTime;
            Debug.LogWarning("timer work");

            if (TempCanvas != null)
            {
                Debug.LogWarning("make it true");
                TempCanvas.SetActive(true);

            }
        }
        // PhaseTxt.text = PhaseValue.ToString();
        if (PhotonNetwork.CurrentRoom != null)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 4)
            {

                StartCoroutine(CounterToStartGame());

            }
        }
        TimerTxt.text = Timer.ToString("f0");
        txtTeam2.text = Rocket1BehaviourMultiplayer.Team2Score.ToString(); /*Rocket1Behaviour.ManTeamScore.ToString();*/
        txtTeam1.text = Rocket1BehaviourMultiplayer.Team1Score.ToString();
        // Debug.LogWarning("number of players" + PhotonNetwork.CurrentRoom.PlayerCount);
        InstantiatePlayers();

        if (Input.GetKeyDown(KeyCode.B))
        {
            //  ChangeText();
            if (photonView.IsMine)
            {

                photonView.RPC("ChangeText", RpcTarget.AllBuffered);

            }
            //  for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            //  {
            //  if (photonView.IsMine)
            //  {
            //  photonView.RPC("ChangeBool", RpcTarget.All,true,false);

            // }

            //  }

        }
        // "back" button of phone equals "Escape". quit app if that's pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitApplication();
        }
        if (GameObject.FindGameObjectWithTag("LocalPlayerCanvas") != null)
        {
            if (countertostartgame == false)
            {
                Debug.LogWarning("make it false");
                TempCanvas = GameObject.FindGameObjectWithTag("LocalPlayerCanvas");
                GameObject.FindGameObjectWithTag("LocalPlayerCanvas").SetActive(false);

            }
        }
    }
    string PlayerLeft()
    {
        if (GameObject.FindGameObjectWithTag("Man")) ;
        else return "Man";
        if (GameObject.FindGameObjectWithTag("PoliceMan")) ;
        else return "PoliceMan";
        if (GameObject.FindGameObjectWithTag("Girl")) ;
        else return "Girl";
        if (GameObject.FindGameObjectWithTag("Zombie")) ;
        else return "Zombie";

        return "";
    }
    public static bool countertostartgame;
    int thecountertostartgame;

    IEnumerator CounterToStartGame()
    {
        thecountertostartgame++;
        if (thecountertostartgame == 1)
        {
            LoadingScreen.SetActive(false);
            MainCanvas.SetActive(true);
            VehicleControllerMultiplayer.setcam = true;
            yield return new WaitForSeconds(2);
            yield return StartCoroutine(CarMode());
            //countertostartgame = true;
            if (Rounds < 5)
            {
                yield return StartCoroutine(getcountDownTimer());
                if (photonView.IsMine)
                    photonView.RPC("StartRound", RpcTarget.All, true);
            }

        }



    }
    IEnumerator getcountDownTimer()
    {
        WinLosetxt.text = "4";
        yield return new WaitForSeconds(1);
        WinLosetxt.text = "3";
        yield return new WaitForSeconds(1);
        WinLosetxt.text = "2";
        yield return new WaitForSeconds(1);
        WinLosetxt.text = "1";
        yield return new WaitForSeconds(1);
        WinLosetxt.text = "0";
        yield return new WaitForSeconds(1);
        WinLosetxt.text = "";



    }
    [PunRPC]
    void StartRound(bool startgame)
    {
        countertostartgame = startgame;
    }
    [PunRPC]
    void ChangeText()
    {
        TextToshowSomeValues.text = ("I Pressed B Key" + " " + " Id = " + photonView.ViewID);
    }
    bool instantiateCar;
    bool startcourotine;
    IEnumerator CarMode()
    {
        Debug.LogWarning("before zeroS");
        if (startcourotine == false)
        {
            Debug.LogWarning("0");

            Rounds++;
            startcourotine = true;
            yield return new WaitForSeconds(1);
            //if (PhaseValue == 0 /*&& instantiateCar == false*/)
            //{
            //    PhaseTxt.text = PhaseValue.ToString() + "  " + "Get the Car";
            //    PhotonNetwork.Instantiate(playerPrefabs[0].name, new Vector3(10, 0, 0), Quaternion.identity);
            //    instantiateCar = true;
            //}
            //if (PhaseValue == 1)
            //{
            //    PhaseTxt.text = PhaseValue.ToString() + "  " + "Get Your Character";
            //}
            if (Rounds == 1)
            {
                photonView.RPC("SendCarModeUpdate", RpcTarget.All, "PoliceMan");

            }
            if (Rounds == 2)
            {
                photonView.RPC("SendCarModeUpdate", RpcTarget.All, "Zombie");

            }
            if (Rounds == 3)
            {
                photonView.RPC("SendCarModeUpdate", RpcTarget.All, "Man");

            }
            if (Rounds == 4)
            {
                photonView.RPC("SendCarModeUpdate", RpcTarget.All, "Girl");

            }
            if (Rounds >= 5)
            {
                Debug.LogWarning("10");
                if (Rocket1BehaviourMultiplayer.Team1Score > Rocket1BehaviourMultiplayer.Team2Score)
                {
                    if (VehicleControllerMultiplayer.playertagnameforwinlose == "Man" || VehicleControllerMultiplayer.playertagnameforwinlose == "PoliceMan")
                    {
                        WinLosetxt.text = "Winner";
                        Instantiate(WinnersoundObject);
                        VehicleControllerMultiplayer.winnerloser = 0;
                    }
                    else
                    {
                        WinLosetxt.text = "Loser";
                        Instantiate(LoserSoundObject);
                        VehicleControllerMultiplayer.winnerloser = 1;
                    }
                }
                if (Rocket1BehaviourMultiplayer.Team1Score < Rocket1BehaviourMultiplayer.Team2Score)
                {
                    if (VehicleControllerMultiplayer.playertagnameforwinlose == "Man" || VehicleControllerMultiplayer.playertagnameforwinlose == "PoliceMan")
                    {
                        WinLosetxt.text = " Loser";
                        Instantiate(LoserSoundObject);
                        VehicleControllerMultiplayer.winnerloser = 1;
                    }
                    else
                    {
                        WinLosetxt.text = "Winner";
                        Instantiate(WinnersoundObject);
                        VehicleControllerMultiplayer.winnerloser = 0;
                    }
                }
                if (Rocket1BehaviourMultiplayer.Team1Score == Rocket1BehaviourMultiplayer.Team2Score)
                {
                    Debug.Log(Team1Counter + " " + Team2Counter);
                    WinLosetxt.text = "Tie";
                    Instantiate(LoserSoundObject);
                    VehicleControllerMultiplayer.winnerloser = 1;

                }
                StartCoroutine(GoToMainMenu());
            }
            // GameObject.FindGameObjectWithTag("PoliceMan").SetActive(false);
            yield return new WaitForSeconds(.5f);
            //   PhotonNetwork.Instantiate(playerPrefabs[0].name, new Vector3(-41f, 0, 12.5f), Quaternion.identity);

        }
        //if (Timer <10 && instantiateCar == true)
        //{
        //    PhotonNetwork.Instantiate(playerPrefabs[2].name, new Vector3(-10, 0, 0), Quaternion.identity);
        //    instantiateCar = false;
        //    PhaseValue = 1;
        //}

        //  VehicleController.LocalPlayerInstance.SetActive(false);

    }
    [PunRPC]
    void PlayerLeaveAgame(string playerleftparam)
    {
        //  if (photonView.IsMine)
        // {
        Timer = -1;

        Debug.LogWarning("leaving room on client " + playerleftparam);
        startcourotine = false;
        switch (playerleftparam)
        {
            case "Man":
                Rounds = 5;
                Rocket1BehaviourMultiplayer.Team2Score = 1;
                Rocket1BehaviourMultiplayer.Team1Score = 0;
                //  StartCoroutine(CarMode());
                StartCoroutine(CounterToStartGame());
                break;
            case "PoliceMan":
                Rounds = 5;
                Rocket1BehaviourMultiplayer.Team2Score = 1;
                Rocket1BehaviourMultiplayer.Team1Score = 0;
                // StartCoroutine(CarMode());
                StartCoroutine(CounterToStartGame());
                break;
            case "Girl":
                Rounds = 5;
                Rocket1BehaviourMultiplayer.Team2Score = 0;
                Rocket1BehaviourMultiplayer.Team1Score = 1;
                //  StartCoroutine(CarMode());
                StartCoroutine(CounterToStartGame());
                break;
            case "Zombie":
                Rounds = 5;
                Rocket1BehaviourMultiplayer.Team2Score = 0;
                Rocket1BehaviourMultiplayer.Team1Score = 1;
                // StartCoroutine(CarMode());
                StartCoroutine(CounterToStartGame());
                break;
        }
        // }

        //  StartCoroutine(GoToMainMenu());
    }
    IEnumerator GoToMainMenu()
    {
        yield return new WaitForSeconds(20);
        Debug.Log("GO out of room");
        PhotonNetwork.LeaveRoom();
        ResetFunction();
        photonView.RPC("ResetFunction", RpcTarget.All);
        yield return new WaitForSeconds(1);

        SceneManager.LoadSceneAsync(0);

    }
    [PunRPC]
    void ResetFunction()
    {
        //if (photonView.IsMine)
        //{
        LocalPlayer = null;
        Team1Counter = 0;
        Team2Counter = 0;
        Rounds = 0;
        countertostartgame = false;
        VehicleControllerMultiplayer.destroy = true;
        thecountertostartgame = 0;
        startcourotine = false;
        Rocket1BehaviourMultiplayer.Team1Score = 0;
        Rocket1BehaviourMultiplayer.Team2Score = 0;

        VehicleControllerMultiplayer.THECAR = null;
        VehicleControllerMultiplayer.LocalPlayerInstance = null;

        VehicleControllerMultiplayer.GameStart = false;
        VehicleControllerMultiplayer.tagname = "";
        VehicleControllerMultiplayer.playertagnameforwinlose = "";
        VehicleControllerMultiplayer.EnableGuradCubeEffect = false;
        VehicleControllerMultiplayer.PlayerName = "";
        VehicleControllerMultiplayer.winnerloser = 2;
        VehicleControllerMultiplayer.setcam = false;
        VehicleControllerMultiplayer.movecar = false;
        VehicleControllerMultiplayer.bakcCar = false;
        VehicleControllerMultiplayer.playerlocalCanvas = null;
        VehicleControllerMultiplayer.LocalCarCanvas = null;
        // }

    }
    [PunRPC]

    void SendCarModeUpdate(string Tagname)
    {
        VehicleControllerMultiplayer.tagname = Tagname;
    }
    int sendRPCTEstCounter;
    void InstantiatePlayers()
    {
        //  if (PhotonNetwork.CurrentRoom.PlayerCount==3)
        //  {
        if (Input.GetKeyDown(KeyCode.I))
        {

        }


        if (GetPlayerPrefab() == null)
        { // #Tip Never assume public properties of Components are filled up properly, always check and inform the developer of it.

            Debug.LogError("<Color=Red><b>Missing</b></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else
        {


            if (VehicleControllerMultiplayer.LocalPlayerInstance == null)
            {
                Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);

                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                int prefabIndex = getRandomNum();
                if (PhotonNetwork.CurrentRoom != null)
                    PhotonNetwork.Instantiate(this.playerPrefabs[PhotonNetwork.CurrentRoom.PlayerCount].name, GetPlayerPos(PhotonNetwork.CurrentRoom.PlayerCount), this.playerPrefabs[PhotonNetwork.CurrentRoom.PlayerCount].transform.rotation, 0);

                //                        if (photonView.IsMine)
                //                        {
                // PhotonNetwork.Instantiate(this.playerPrefabs[1].name, GetPlayerPos(1), this.playerPrefabs[1].transform.rotation, 0);

                //                        }
                //                        else
                //PhotonNetwork.Instantiate(this.playerPrefabs[2].name, GetPlayerPos(2), this.playerPrefabs[2].transform.rotation, 0);

                //                    }
                //else
                //{

                //    Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
                //}


            }
            //  }
        }
        if (PhotonNetwork.CurrentRoom != null && PhotonNetwork.CurrentRoom.PlayerCount == 4 && sendRPCTEstCounter == 0)
        {
            sendRPCTEstCounter++;
            if (sendRPCTEstCounter == 1)
            {
                photonView.RPC("StartGame", RpcTarget.All, true);

            }
        }
    }
    [PunRPC]
    public void ChangeBool(bool v1, bool v2)
    {

        char2 = v1;
        char1 = v2;


    }
    [PunRPC]
    public void ShowNumOfPlayers(int numofplayers)
    {

        NumOFplayer = numofplayers;


    }
    #endregion
    [PunRPC]
    public void StartGame(bool gamestart)
    {
        VehicleControllerMultiplayer.GameStart = gamestart;

    }
    #region Photon Callbacks

    /// <summary>
    /// Called when a Photon Player got connected. We need to then load a bigger scene.
    /// </summary>
    /// <param name="other">Other.</param>
    public override void OnPlayerEnteredRoom(Player other)
    {

        Debug.Log("Player Entered Room");           // photonView.RPC("ShowNumOfPlayers", RpcTarget.AllBuffered, NumOFplayer);
        if (PhotonNetwork.CurrentRoom.PlayerCount == 4)
        {
            VehicleControllerMultiplayer.setcam = true;
        }
        //if (NumOFplayer==1)
        //{
        //    PhotonNetwork.Instantiate(this.playerPrefabs[getRandomNum()].name, GetPlayerPos(prefab), Quaternion.identity, 0);

        //}
        Debug.Log("OnPlayerEnteredRoom() " + other.NickName); // not seen if you're the player connecting

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

            LoadArena();
        }
    }

    /// <summary>
    /// Called when a Photon Player got disconnected. We need to load a smaller scene.
    /// </summary>
    /// <param name="other">Other.</param>
    public override void OnPlayerLeftRoom(Player other)
    {
        Debug.Log("OnPlayerLeftRoom() " + other.NickName); // seen when other disconnects

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

            LoadArena();
        }
    }

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public static string playerleft;
    public override void OnLeftRoom()
    {
        //  playerleft = VehicleController.playertagnameforwinlose;
        //    photonView.RPC("playerleftroom", RpcTarget.All, VehicleController.playertagnameforwinlose);
        SceneManager.LoadScene("PunBasics-Launcher");
    }
    [PunRPC]
    void playerleftroom(string playertag)
    {
        playerleft = playertag;
        Debug.LogWarning("player left  = " + playertag);
    }

    #endregion

    #region Public Methods

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    #endregion

    #region Private Methods

    void LoadArena()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
        }

        Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);

        PhotonNetwork.LoadLevel("PunBasics-Room for " + 2/*PhotonNetwork.CurrentRoom.PlayerCount*/);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            // stream.SendNext(this.char2);
            // stream.SendNext(this.char1);
            //  stream.SendNext(this.NumOFplayer);
            //stream.SendNext(this.PhaseValue);
            // stream.SendNext(this.txtTeam1.text);
            // stream.SendNext(this.txtTeam2.text);
        }
        else
        {
            // Network player, receive data
            //   this.char2 = (bool)stream.ReceiveNext();
            // this.char1 = (bool)stream.ReceiveNext();
            // this.NumOFplayer = (int)stream.ReceiveNext();
            // this.PhaseValue = (int)stream.ReceiveNext()
            //this.txtTeam1.text = stream.ReceiveNext().ToString();
            // this.txtTeam2.text = stream.ReceiveNext().ToString(); ;
            ;
        }
    }

    #endregion
    public override void OnJoinedRoom()
    {

        Debug.LogWarning("Player Joined Room");

        //GameObject obj = PhotonNetwork.Instantiate(P1.name, source_host.transform.position, Quaternion.identity, 0);
        //if (PhotonNetwork.IsMasterClient)
        //{
        //    obj.tag = "PMaster";
        //    obj.transform.position = source_host.transform.position;

        //}
        //else
        //{
        //    obj.tag = "PClient";
        //    obj.transform.position = source_client.transform.position;
        //}
    }
    [PunRPC]
    void SendPhaseValues()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhaseValue = 0;
        }
        else
        {
            PhaseValue = 1;
        }

    }
}

