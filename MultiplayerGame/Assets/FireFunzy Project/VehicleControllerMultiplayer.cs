﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Photon.Pun;
using UnityEngine.Experimental.AI;

public class VehicleControllerMultiplayer : MonoBehaviourPunCallbacks, IPunObservable
{
    public static Transform THECAR;
    public float speed, rotationSpeed;
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    [SerializeField] GameObject Rocket1;
    public static bool GameStart;
    public static string tagname;
    public static string playertagnameforwinlose;
    public GameObject GuardCubeEffect;
    public static bool EnableGuradCubeEffect;
    bool instCanvas;
    public static string PlayerName;
    [SerializeField] Text playerNameTxt;


    [SerializeField] GameObject CharacterAnimator, fireEffect, FireAudioObject;


    public static int winnerloser = 2;//0 == win 1 == Lose 

    public static bool destroy;

    private Slider slider;
    float currentvalue;

    private void Awake()
    {
        if (photonView.IsMine)
        {
            destroy = false;
            sendRPCTEstCounter = 0;
            instantiateCar = 0;

            //VehicleController.THECAR = null;
            //VehicleController.LocalPlayerInstance = null;

            //VehicleController.GameStart = false;
            //VehicleController.tagname = "";
            //VehicleController.playertagnameforwinlose = "";
            //VehicleController.EnableGuradCubeEffect = false;
            //VehicleController.PlayerName = "";
            //VehicleController.winnerloser = 2;
            //VehicleController.setcam = false;
            //VehicleController.movecar = false;
            //VehicleController.bakcCar = false;
            //VehicleController.playerlocalCanvas = null;
            //VehicleController.LocalCarCanvas = null;
        }












        Debug.LogWarning("Awake is happenning");
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            if (this.tag != "Car")
                playerNameTxt.text = PlayerName;

            LocalPlayerInstance = gameObject;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(gameObject);
    }
    [SerializeField] GameObject ManCanvas, CarCanvas, PoliceCanvas, GirlCanvas, ZombieCanvas;
    private void Start()
    {

        if (photonView.IsMine)
        {

            currentvalue = .5f;
            playerNameTxt.text = PlayerName;
            photonView.RPC("showPlayerName", RpcTarget.AllBufferedViaServer, PlayerName);

        }

        instCanvas = false;
        Debug.Log("tag= " + this.gameObject.tag);


    }

    public static bool setcam;
    void SetCamPos()
    {
        setcam = false;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().transform.position = new Vector3(-20.34f, 6.9f, this.transform.position.z);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().transform.rotation = Quaternion.Euler(33.7f, 270, 0); ;
    }
    [PunRPC]
    void showPlayerName(string playername)
    {
        playerNameTxt.text = playername;
    }
    private void OnEnable()
    {
        instCanvas = false;

        Debug.LogWarning("onEnable");
        if (photonView.IsMine)
        {
            if (this.tag == "Car")
            {
                if (playerlocalCanvas != null)
                {
                    playerlocalCanvas.transform.GetChild(0).gameObject.SetActive(false);
                    playerlocalCanvas.transform.GetChild(2).gameObject.SetActive(true);
                    //   playerlocalCanvas.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(MoveCar);
                    //  playerlocalCanvas.transform.GetChild(2).transform.GetChild(1).GetComponent<Button>().onClick.AddListener(BrakeCar);
                    // playerlocalCanvas.transform.GetChild(2).transform.GetChild(0).GetComponent<EventTrigger>().OnPointerUp.AddListener(() =>);
                }
            }
            else
            {
                if (playerlocalCanvas != null)
                {
                    playerlocalCanvas.transform.GetChild(0).gameObject.SetActive(true);
                    playerlocalCanvas.transform.GetChild(2).gameObject.SetActive(false);
                }
            }
        }

    }
    public void ManButton()
    {
        StartCoroutine(manageFireEffect());

        PhotonNetwork.Instantiate(FireAudioObject.name, this.transform.position, Quaternion.identity);
        ColorBlock colorblock = playerlocalCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().colors;
        colorblock.normalColor = Color.black;
        playerlocalCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().colors = colorblock;
        playerlocalCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().enabled = false;
        if (photonView.IsMine)
        {

        }
        CharacterAnimator.GetComponent<Animator>().SetBool("Fire", true);
        photonView.RPC("FireAnimation", RpcTarget.All, this.tag, true);
        var Rocket = PhotonNetwork.Instantiate(Rocket1.name, this.transform.position, CharacterAnimator.transform.rotation);
        //   Rocket.tag = this.gameObject.tag;
        photonView.RPC("TagRocket", RpcTarget.All, this.gameObject.tag);
        StartCoroutine(DisableAnimation());
        StartCoroutine(ReturnButton());
    }
    IEnumerator manageFireEffect()
    {
        //fireEffect.SetActive(true);
        var effect = PhotonNetwork.Instantiate(fireEffect.name, fireEffect.transform.position, Quaternion.identity);

        yield return new WaitForSeconds(1.5f);
        PhotonNetwork.Destroy(effect);

    }
    [PunRPC]
    void FireAnimation(string playerTag, bool AnimValue)
    {
        GameObject.FindGameObjectWithTag(playerTag).transform.GetChild(0).GetComponent<Animator>().SetBool("Fire", AnimValue);
    }
    [PunRPC]
    void DanceAnimation(string playerTag, string paramName, bool AnimValue)
    {
        GameObject.FindGameObjectWithTag(playerTag).transform.GetChild(0).GetComponent<Animator>().SetBool(paramName, AnimValue);
    }


    IEnumerator ReturnButton()
    {
        yield return new WaitForSeconds(1);
        ColorBlock colorblock = playerlocalCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().colors;
        colorblock.normalColor = Color.white;
        playerlocalCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().colors = colorblock;
        playerlocalCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().enabled = true;
    }
    [PunRPC]
    void TagRocket(string tag)
    {
        Rocket1BehaviourMultiplayer.thisRocketTag = tag;
    }
    IEnumerator DisableAnimation()
    {
        yield return new WaitForSeconds(.01f);
        CharacterAnimator.GetComponent<Animator>().SetBool("Fire", false);
        photonView.RPC("FireAnimation", RpcTarget.All, this.tag, false);


    }

    public void CarButton()
    {

    }
    int sendRPCTEstCounter;
    int instantiateCar;
    void Update()
    {

        if (destroy)
        {
            PhotonNetwork.Destroy(this.gameObject);
        }
        if (photonView.IsMine)
        {
            if (winnerloser == 0)
            {
                Debug.Log("Girl Win 0" + this.tag);
                if (this.tag == "Man" || this.tag == "PoliceMan")
                {
                    CharacterAnimator.GetComponent<Animator>().SetBool("Dance2", true);
                    photonView.RPC("DanceAnimation", RpcTarget.All, this.tag, "Dance2", true);

                }
                if (this.tag == "Girl" || this.tag == "Zombie")
                {

                    Debug.Log("Girl Win");
                    CharacterAnimator.GetComponent<Animator>().SetBool("Dance1", true);
                    photonView.RPC("DanceAnimation", RpcTarget.All, this.tag, "Dance1", true);

                }
            }
            if (winnerloser == 1)
            {
                Debug.Log("Girl Win 0" + this.tag);
                if (this.tag == "Man" || this.tag == "PoliceMan")
                {
                    CharacterAnimator.GetComponent<Animator>().SetBool("Cry1", true);
                    photonView.RPC("DanceAnimation", RpcTarget.All, this.tag, "Cry1", true);

                }
                if (this.tag == "Girl" || this.tag == "Zombie")
                {

                    Debug.Log("Girl Win");
                    CharacterAnimator.GetComponent<Animator>().SetBool("Cry2", true);
                    photonView.RPC("DanceAnimation", RpcTarget.All, this.tag, "Cry2", true);

                }
            }
        }

        if (photonView.IsMine)
        {
            if (setcam) SetCamPos();
            playertagnameforwinlose = this.gameObject.tag;
            Debug.Log("Update tag = " + this.gameObject.tag);
            var Vertical = Input.GetAxis("Vertical") * speed;
            if (this.tag == "Car")
            {

                //  transform.Translate(new Vector3(0, 0, Vertical));
                if (movecar)
                {
                    Debug.Log("MOVE CAR");
                    this.transform.Translate(Vector3.forward * Time.deltaTime * 15);

                }
                if (bakcCar)
                {
                    this.transform.Translate(-Vector3.forward * Time.deltaTime * 15);

                }
            }
            else
            {
                if (playerlocalCanvas != null)
                {
                    //playerlocalCanvas.transform.GetChild(0).gameObject.SetActive(true);
                    //playerlocalCanvas.transform.GetChild(2).gameObject.SetActive(false);
                }
            }
            // transform.Rotate(0, Horizontal, 0);
            if (playerlocalCanvas != null)
            {
                slider = playerlocalCanvas.transform.GetChild(1).transform.GetChild(0).GetComponent<Slider>();// Input.GetAxis("Horizontal") * rotationSpeed;
                if (Input.GetMouseButtonUp(0))
                {
                    Debug.Log("mouse up");
                    slider.value = .5f;
                    movecar = false;
                    bakcCar = false;
                }
                if (currentvalue < slider.value)
                {
                    Debug.Log("Rotate Right");
                    if (this.tag == "Car") CharacterAnimator.transform.Rotate(0, 4, 0);
                    else CharacterAnimator.transform.Rotate(0, .5f, 0);
                }
                if (currentvalue > slider.value)
                {
                    Debug.Log("Rotate Left");
                    if (this.tag == "Car") CharacterAnimator.transform.Rotate(0, -4, 0);
                    else CharacterAnimator.transform.Rotate(0, -.5f, 0);

                }
                //  transform.Rotate(0, Horizontal, 0);

            }
            if (EnableGuradCubeEffect)
            {

                if (GameObject.FindGameObjectWithTag("Car") != null) GameObject.FindGameObjectWithTag("Car").GetComponent<VehicleControllerMultiplayer>().GuardCubeEffect.SetActive(true);//       GuardCubeEffect.SetActive(true);


            }
            if (GameStart)
            {
                sendRPCTEstCounter++;
                if (sendRPCTEstCounter == 5)
                {
                    instantiateCanvas();

                }


            }
            if (this.gameObject.tag == tagname)
            {
                instantiateCar++;
                if (instantiateCar == 1)
                {
                    GameManagerMultiplayer.LocalPlayer = this.gameObject;
                    var car = PhotonNetwork.Instantiate("Vehicle03_Dodge", new Vector3(-41f, 0, 12.5f), Quaternion.identity);
                    SetCamCar();
                    // THECAR = car.transform;
                    //    photonView.RPC("AssignTheCar", RpcTarget.All, car.transform);
                    playerlocalCanvas.SetActive(false);
                    //    this.gameObject.SetActive(false);
                    //var Canvas = Instantiate(CarCanvas);
                    //LocalCarCanvas = Canvas; 
                    //Canvas.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ManButton);
                    photonView.RPC("SendCarModeUpdate", RpcTarget.All, this.gameObject.tag);
                }
            }
        }
        // }

    }



    void SetCamCar()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().transform.SetPositionAndRotation(GameObject.Find("CarCamPos").transform.position, GameObject.Find("CarCamPos").transform.rotation);

    }
    public static bool movecar, bakcCar;
    public void MoveCar()
    {
        Debug.Log("button pressed");
        movecar = true;
    }
    public void StopCar()
    {

    }
    public void BrakeCar()
    {
        bakcCar = true;
    }















    [PunRPC]
    void AssignTheCar(Transform cartransform)
    {
        THECAR = cartransform;
    }
    [PunRPC]

    void SendCarModeUpdate(string Tagname)
    {
        GameObject.FindGameObjectWithTag(tagname).GetComponent<VehicleControllerMultiplayer>().enabled = false; // Here to Disable  Movement of the player during using the car 
        if (photonView.IsMine)
        {
            if (this.tag == "Car")
            {
                if (playerlocalCanvas != null)
                {
                    playerlocalCanvas.transform.GetChild(0).gameObject.SetActive(false);
                    playerlocalCanvas.transform.GetChild(2).gameObject.SetActive(true);
                    //   playerlocalCanvas.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(MoveCar);
                    //  playerlocalCanvas.transform.GetChild(2).transform.GetChild(1).GetComponent<Button>().onClick.AddListener(BrakeCar);
                    // playerlocalCanvas.transform.GetChild(2).transform.GetChild(0).GetComponent<EventTrigger>().OnPointerUp.AddListener(() =>);
                }
            }
        }
        for (int i = 0; i < GameObject.FindGameObjectWithTag(tagname).transform.childCount; i++)
        {
            GameObject.FindGameObjectWithTag(tagname).transform.GetChild(i).gameObject.SetActive(false);
            //if (this.gameObject.tag == tagname)
            //{
            //    if (GameObject.FindGameObjectWithTag("LocalPlayerCanvas") != null)
            //    {
            //        GameObject.FindGameObjectWithTag("LocalPlayerCanvas").SetActive(false);
            //    }
            //}

        }
    }
    public static GameObject playerlocalCanvas, LocalCarCanvas;

    void instantiateCanvas()
    {
        if (this.gameObject.tag == "Man")
        {
            GameStart = false;
            var Canvas = Instantiate(ManCanvas);
            playerlocalCanvas = Canvas;
            Canvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ManButton);
        }
        if (this.gameObject.tag == "PoliceMan")
        {
            GameStart = false;
            var Canvas = Instantiate(PoliceCanvas);
            playerlocalCanvas = Canvas;
            Canvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ManButton);
        }
        if (this.gameObject.tag == "Girl")
        {
            GameStart = false;
            var Canvas = Instantiate(GirlCanvas);
            playerlocalCanvas = Canvas;
            Canvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ManButton);
        }
        if (this.gameObject.tag == "Zombie")
        {
            GameStart = false;
            var Canvas = Instantiate(ZombieCanvas);
            playerlocalCanvas = Canvas;
            Canvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ManButton);
        }
        if (photonView.IsMine)
        {
            if (this.tag == "Car")
            {
                if (playerlocalCanvas != null)
                {
                    playerlocalCanvas.transform.GetChild(0).gameObject.SetActive(false);
                    playerlocalCanvas.transform.GetChild(2).gameObject.SetActive(true);
                    //   playerlocalCanvas.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(MoveCar);
                    //  playerlocalCanvas.transform.GetChild(2).transform.GetChild(1).GetComponent<Button>().onClick.AddListener(BrakeCar);
                    // playerlocalCanvas.transform.GetChild(2).transform.GetChild(0).GetComponent<EventTrigger>().OnPointerUp.AddListener(() =>);
                }
            }
        }

    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(this);
            //stream.SendNext(this.Health);
        }
        else
        {
            // Network player, receive data
            //   this = (bool)stream.ReceiveNext();
            // this = (float)stream.ReceiveNext();
        }
    }
}

