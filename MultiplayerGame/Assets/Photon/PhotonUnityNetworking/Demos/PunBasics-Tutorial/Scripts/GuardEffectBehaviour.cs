﻿using Photon.Pun.Demo.PunBasics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardEffectBehaviour : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(Deactivate());
    }
    IEnumerator Deactivate ()
    {
        yield return new WaitForSeconds(3);
        VehicleController.EnableGuradCubeEffect = false;
        this.gameObject.SetActive(false);
    }
}
