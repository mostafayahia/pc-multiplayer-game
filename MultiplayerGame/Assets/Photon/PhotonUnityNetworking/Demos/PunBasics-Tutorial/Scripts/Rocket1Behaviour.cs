﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Photon.Pun.Demo.PunBasics
{
    public class Rocket1Behaviour : MonoBehaviourPunCallbacks, IPunObservable
    {
        public static Transform CarTransform;
        public static int Team1Score,Team2Score;
        [SerializeField] GameObject HitEffect,CarHitAudEffect;
        public static  string thisRocketTag; 
        private void Awake()
        {
       
            CarTransform = GameManager.Instance.playerPrefabs[0].transform;
            Debug.Log("Car Pos = " + GameManager.Instance.playerPrefabs[0].transform.position);
            VehicleController.THECAR = GameObject.FindGameObjectWithTag("Car").transform; 
        }
        private void Start()
        {
    //   transform.LookAt(VehicleController.THECAR);
            this.gameObject.tag = thisRocketTag;
        }

        // Update is called once per frame
        void Update()
        {
         transform.Translate(Vector3.forward );
        }
        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            throw new System.NotImplementedException();
        }
        public Collider guardCubeCollider; 
        private void OnTriggerEnter(Collider other)
        {
            if (photonView.IsMine)
            {
                if (other.tag == "Car")
                {

                    PhotonNetwork.Instantiate(HitEffect.name, transform.position, Quaternion.identity);
                    PhotonNetwork.Instantiate(CarHitAudEffect.name, transform.position, Quaternion.identity);

                    if (this.gameObject.tag == "Man" || this.gameObject.tag == "PoliceMan")
                    {
                        if (VehicleController.EnableGuradCubeEffect)
                            ;
                        else
                        {
                            if (GameManager.Rounds == 2 || GameManager.Rounds == 4)
                            {
                                Team1Score++;
                            }
                        }
                          

                        photonView.RPC("TeamScore", RpcTarget.All, Team1Score , Team2Score);
                        Debug.Log(this.gameObject.tag + Team1Score);
                    }
                    if (this.gameObject.tag == "Girl" || this.gameObject.tag == "Zombie")
                    {
                        if (VehicleController.EnableGuradCubeEffect)
                            ;
                        else
                        {
                            if (GameManager.Rounds == 1 || GameManager.Rounds == 3)
                            {
                                Team2Score++;
                            }
                        }
                        photonView.RPC("TeamScore", RpcTarget.All,Team1Score, Team2Score);

                        Debug.Log(this.gameObject.tag + Team2Score);
                    }
                    Debug.Log("my tag = " + this.gameObject.tag);
                    //  Team1Score++;
                    Destroy(this.gameObject);
                }
                if (other.tag == "GuardCube")
                {
                    guardCubeCollider = other;
            
                    Destroy(this.gameObject);
                    Destroy(other.gameObject);
                    photonView.RPC("GuardCube", RpcTarget.All, other.GetComponent<PhotonView>().ViewID);
                }
            }
          



        }
        [PunRPC]
        void TeamScore (int team1score ,int team2score)
        {
            Debug.Log("team 2 score  = " + team2score);
            Team1Score = team1score;
            Team2Score = team2score;
            Destroy(this.gameObject);
        }
     [PunRPC]
     void GuardCube (int  id )
        {
            Debug.Log("id = " + id);
            if (GameManager.Rounds == 1 || GameManager.Rounds == 3)
            {
                if (this.gameObject.tag == "Man" || this.gameObject.tag == "PoliceMan")
                {
                    VehicleController.EnableGuradCubeEffect = true;

                }
            }
            if (GameManager.Rounds == 2 || GameManager.Rounds == 4)
            {
                if (this.gameObject.tag == "Girl" || this.gameObject.tag == "Zombie")
                {
                    VehicleController.EnableGuradCubeEffect = true;

                }
            }
            //guardCubeCollider = Other; 
            Destroy(PhotonView.Find(id).gameObject);
            Destroy(this.gameObject);
     
               
        }
    }
}